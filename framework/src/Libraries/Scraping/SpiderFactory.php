<?php
namespace Framework\Libraries\Scraping;

/**
 * @author aduartem
 */

class SpiderFactory
{
    public static function build($spiderName)
    {
        $class = "\\App\Libs\Spider\\" . ucfirst($spiderName);
        
        if(class_exists($class))
            return new $class();
        else
            return NULL;
    }
}