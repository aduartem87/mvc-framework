<?php
namespace Framework\Libraries\Rest;

/**
 * @author aduartem
 */

class RestClient
{
    private $_uri;

    public function __construct($config = NULL)
    {
        $this->_uri = ( ! empty($config['uri'])) ? $config['uri'] : '';
    }

    public function setUri($config)
    {
        $this->_uri = $config['uri'];
    }

    /**
     * Obtiene los datos de todos los recursos o uno en particular.
     *
     * @param int $id ID del recurso (Opcional)
     *
     * @return mixed Retorna la respuesta del servicio
     */
    public function fetch($id = NULL)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

        if( ! empty($id))
            $url = $this->_uri . "{$id}";
        else
            $url = $this->_uri;

        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Actualiza o crea un recurso
     *
     * @param string $aParams Array asociativo con los parámetros de entrada
     * @param int $id ID del recurso a actualizar (Opcional)
     *
     * @return mixed Retorna la respuesta del servicio
     */
    public function save($aParams = array(), $id = NULL)
    {
        if(empty($aParams)) return NULL;

        if( ! empty($id))
        {
            $ch = curl_init($this->_uri . "{$id}");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");            
        }
        else
        {
            $ch = curl_init($this->_uri);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($aParams));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Elimina un recurso
     *
     * @param int $id ID del recurso (Opcional)
     *
     * @return mixed Retorna la respuesta del servicio
     */
    public function delete($id)
    {
        $curl = curl_init($this->_uri . "{$id}");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curl);
        return $response;
    }
}