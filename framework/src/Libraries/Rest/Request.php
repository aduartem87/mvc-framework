<?php
namespace Framework\Libraries\Rest;

/**
 * @author aduartem
 */

class Request
{
    public function __construct(){}

    public function parse()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            if( ! empty($_SERVER['QUERY_STRING']))
                parse_str($_SERVER['QUERY_STRING'], $input);    
            else
                $input = true;
        }
        else
        {
            // Parse body for HTTP verbs post, put and delete
            $requestHeaders = $this->getRequestHeaders();
            switch($requestHeaders['Content-Type'])
            {
                case 'application/json':
                    $input = json_decode(file_get_contents('php://input'), TRUE);
                break;
                case 'application/x-www-form-urlencoded':
                    parse_str(file_get_contents('php://input'), $input);
                break;
                default:
                    $input = FALSE;
                    if(strlen(file_get_contents('php://input')) === 0)
                        $input = TRUE;
            }            
        }
        return $input;
    }

    public function getRequestHeaders()
    {
        $headers = array();
        foreach($_SERVER as $key => $val) 
        {
            if (substr($key, 0, 5) <> 'HTTP_') 
            {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $val;
        }
        return $headers;
    }
}