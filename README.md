# MVC Framework

## Versión PHP

Versión >= 5.6.x

## Instalación

**Clonar el proyecto:**

```sh
$ git clone {url}
```

**Instalar composer:**

En la raiz del proyecto ejecutar los siguientes comandos:

```sh
$ composer install
```

**Nota:** Es requisito tener previamente instalado composer de forma global.

## Estructura de directorios

- app

- framework

- public

- .gitignore

- .htaccess

- README.md

- composer.example.json

- composer.json

- index.php

### app

En esta carpeta se encuentra la estructura de directorios de la aplicación. 

### framework

En esta carpeta se encuentra el core del framework.

### public

La carpeta public contiene todos los archivos que serán de acceso público como por ejemplo: css, js, imágenes, etc.

## Implementación de patrones de diseño

En el micro framework se implemento los siguientes patrones:

- Front Controller

- MVC

- Factory

- Strategy

- Dependency injection

- Active Record

## ORM

El marco de trabajo cuenta con un ORM (mapeo de objeto-relacional) minimalista, que implementa el patrón Active Record a través de la clase QueryBuilder y cuenta con los métodos básicos para realizar consultas a la base de datos. Los métodos del active record evitan los ataques de inyección de SQL ya que utilizan sentencias preparadas.

Un modelo siempre hará referencia a una tabla de la base de datos. Todos los módelos heredan los métodos del ORM.

### Métodos:

**select - Permite definir que campos se van a mostrar** 

**get - Ejecuta una consulta retornando los datos en un array asociativo** 

Recibe un array como parámetro de entrada opcional

Ejemplo1:

```php
$oUsuario = new Usuario();
$aUsuarios = $oUsuario->select()
                	  ->get();
```

Ejemplo2:

```php
$oUsuario = new Usuario();
$aUsuarios = $oUsuario->select(array('email', 'nombre'))
                	  ->get();
```

**where - Define las condiciones de la consulta** 

Ejemplo:

```php
$oUsuario = new Usuario();
$aUsuarios = $oUsuario->select(array('email', 'nombre'))
                	  ->where(array('id' => 6))
                	  ->get();
```

**like - Permite buscar un patron especificado en una columna** 

Ejemplo:

```php
$oUsuario = new Usuario();
$aUsuarios = $oUsuario->select(array('email', 'nombre'))
                ->where()
                ->like('email', 'gmail')
                ->get();
```

**fetchObj - Búsca registros de una tabla, retorna el resultado como un array de objetos** 

```php
$obj::fetchObj(); // Trae todos los registros de una tabla

$obj::fetchObj($aParams); // Al enviar como parámetro un array asociativo, el método filtrará los registros que corresponden a los parámetros enviados.
```

**fetchArray - Búsca registros de una tabla, retorna el resultado como un array asociativo** 

```php
$obj::fetchArray(); // Trae todos los registros de una tabla

$obj::fetchArray($aParams); // Al enviar como parámetro un array asociativo, el método filtrará los registros que corresponden a los parámetros enviados.
```

**save - Inserta o actualiza un registro de una tabla** 

```php
$obj::save($aParams); // Inserta un registro

$obj::save($aParams, $id); // actualiza un registro
```

**delete - Elimina un registro de una tabla** 

```php
$obj::delete($aParams); // elimina un registro
```

## Permisos, propietarios y grupos para Apache

```sh
$ sudo chown -Rvf $USER:www-data mvc-framework
$ sudo chmod 0775 -R mvc-framework
```

Agregar el grupo www-data al usuario actual:

```sh
$ sudo usermod -a -G www-data $USER
```

**Nota:** Esto no es necesario si el servidor utiliza Nginx como Web Server

## Instalar paquetes PHP recomendados

Abrir composer.example.json y copiar en composer.json los repositorios que quiera utilizar. Luego guardar el archivo y ejecutar el siguiente comando en la raiz del proyecto:

```sh
$ composer update
```