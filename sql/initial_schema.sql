CREATE DATABASE demo CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE usuario(
	id INT(11) NOT NULL auto_increment, 
	email VARCHAR(250) NOT NULL, 
	password VARCHAR(150) NOT NULL, 
	nombre VARCHAR(50) NOT NULL, 
	ape_paterno VARCHAR(50) NOT NULL, 
	ape_materno VARCHAR(50) NOT NULL, 
	PRIMARY KEY(id), 
	CONSTRAINT user_unique UNIQUE (email)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;