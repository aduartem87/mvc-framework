<?php
namespace App\Controller;

use Framework\Libraries\Scraping\SpiderFactory;
use Framework\Libraries\Scraping\SpiderController;

/**
 * @author aduartem
 */

class CliController
{
    /**
     * Example:
     * php index.php spider paris
     * php index.php make_model perro
     * php index.php make_model gato tbl_gato
     * php index.php make_controller persona
     */
    public static function main(array $arg = array())
    {
        if( ! isCLI()) die("Debe correr este programa desde línea de comandos.");

        if(empty($arg[1])) die("Falta parámetro para CliController::main()");

        switch($arg[1])
        {
            case 'make_model':
                $class = ( ! empty($arg[2])) ? $arg[2] : '';
                $table = ( ! empty($arg[3])) ? $arg[3] : '';
                self::makeModel($class, $table);
                break;
            case 'make_controller':
                self::makeController($arg[2]);
                break;
            case 'spider':
                self::spider($arg[2]);
                break;
            default:
                echo 'cook main';
                break;
        }
    }

    private static function makeModel($className, $tableName = '')
    {
        $className  = ucfirst($className);
        $fileName   = MODEL_PATH . $className . ".php";

        if(file_exists($fileName) && is_readable($fileName))
        {
            echo 'File exists';
            return;
        }
        elseif( ! is_readable($fileName))
        {
            echo "The file exists but is not readable";
            return;
        }

        $fh = fopen($fileName, "w");
        fwrite($fh, "<?php" . PHP_EOL);
        fwrite($fh, "namespace App\Model;" . PHP_EOL . PHP_EOL);
        fwrite($fh, "use Framework\ORM\Model;" . PHP_EOL . PHP_EOL);
        fwrite($fh, "class ".$className." extends Model" . PHP_EOL);
        fwrite($fh, "{" . PHP_EOL);
        $var = "$";
        fwrite($fh, "\tprotected static ".$var."table = '".$tableName."';" . PHP_EOL . PHP_EOL);
        fwrite($fh, "\tpublic function __construct(){}" . PHP_EOL);
        fwrite($fh, "}" . PHP_EOL);
        fclose($fh);

        if( ! file_exists($fileName))
        {
            echo 'The file was not created.';
            return;
        }

        if( ! is_readable($fileName))
        {
            echo "The file is not readable";
            return;
        }

        echo 'The file was successfully created.';
    }

    private static function makeController($className)
    {
        $className  = ucfirst($className) . "Controller";
        $fileName   = CONTROLLER_PATH . $className . ".php";

        if(file_exists($fileName) && is_readable($fileName))
        {
            echo 'File exists';
            return;
        }
        elseif( ! is_readable($fileName))
        {
            echo "The file exists but is not readable";
            return;
        }

        $fh = fopen($fileName, "w");
        fwrite($fh, "<?php" . PHP_EOL);
        fwrite($fh, "namespace App\Controller;" . PHP_EOL . PHP_EOL);
        fwrite($fh, "class ".$className." extends AppController" . PHP_EOL);
        fwrite($fh, "{" . PHP_EOL);
        fwrite($fh, "\tpublic function __construct()" . PHP_EOL);
        fwrite($fh, "\t{" . PHP_EOL);
        fwrite($fh, "\t\tparent::__construct();" . PHP_EOL);
        fwrite($fh, "\t}" . PHP_EOL . PHP_EOL);
        fwrite($fh, "\tpublic function index()" . PHP_EOL);
        fwrite($fh, "\t{" . PHP_EOL);
        fwrite($fh, "\t\t" . PHP_EOL);
        fwrite($fh, "\t}" . PHP_EOL);
        fwrite($fh, "}" . PHP_EOL);
        fclose($fh);

        if( ! file_exists($fileName))
        {
            echo 'The file was not created.';
            return;
        }

        if( ! is_readable($fileName))
        {
            echo "The file is not readable";
            return;
        }

        echo 'The file was successfully created.';
    }

    private static function spider($spiderName)
    {
        if( empty($spiderName))
        {
            echo "missing parameter." . PHP_EOL;
            return;
        }

        $oSpider = SpiderFactory::build($spiderName);

        if( ! is_null($oSpider))
            $oSpider->run();
        else
            echo "does not exist: " . $spiderName . PHP_EOL;
    }
}