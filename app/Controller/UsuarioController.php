<?php
namespace App\Controller;

use App\Model\Usuario;

/**
 * @author aduartem
 */

class UsuarioController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $oUsuario = new Usuario();

        // trae todos
        /*$aUsuarios = $oUsuario->select(array('email', 'nombre'))
                ->get();*/

        // trae filtrando datos
        /*$aUsuarios = $oUsuario->select(array('email', 'nombre'))
                ->where(array('id' => 6))
                ->get();*/

        // busqueda
        /*$aUsuarios = $oUsuario->select(array('email', 'nombre'))
                        ->where()
                        ->like('email', 'gmail')
                        ->get();*/

        // trae todos
        $oUsuarios = $oUsuario::fetchObj();

        foreach($oUsuarios as $usuario)
        {
            echo $usuario->id . ' ' . $usuario->email . ' ' . $usuario->nombre . ' ' . $usuario->ape_paterno . '<br>';
        }

        //$aUsuarios = $oUsuario::fetchArray();

        // trae filtrando datos
        //$aUsuarios = Usuario::find(array('id' => 3));

        // inserta registro
        /*$aUsuarios = Usuario::save(array(
        	'email' 		=> 'jrduarte@gmail.com',
        	'password' 		=> 'asdf',
        	'nombre' 		=> 'Rolando',
        	'ape_paterno' 	=> 'Duarte',
        	'ape_materno'	=> 'Verdugo'
    	));*/

    	// actualiza registro
    	/*$aUsuarios = Usuario::save(array(
        	'email' 		=> 'aduarte@gmail.com',
        	'password' 		=> '123',
        	'nombre' 		=> 'Andres Erasmo',
        	'ape_paterno' 	=> 'Duarte',
        	'ape_materno'	=> 'Márquez'
    	), 6);*/

    	// elimina registro
    	//$aUsuarios = Usuario::delete(array('id' => 1));
        //echo '<pre>'; print_r($aUsuarios);
    }
}