<?php
namespace App\Controller;

/**
 * @author aduartem
 */

class WelcomeController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $aData['VIEW']   = 'welcome/index';
        $aData['nombre'] = 'Andres';
        $this->layout($aData);
    }

    public function example()
    {
        $this->layout();
    }

    public function obtenerNombres()
    {
        $aData = array(
            array('nombre' => 'Andres', 'apellido' => 'Duarte'),
            array('nombre' => 'Rolando', 'apellido' => 'Duarte'),
            array('nombre' => 'Cecilia', 'apellido' => 'Márquez')
        );

        $this->json(array(
            'status'  => 'OK',
            'message' => '',
            'data'    => $aData
        ));
    }
}