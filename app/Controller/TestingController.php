<?php
namespace App\Controller;

use Framework\Libraries\Rest\RestClient;
use Framework\Libraries\Upload;

/**
 * @author aduartem
 */

class TestingController extends AppController
{
    public function __construct()
    {
        parent::__construct();

        $this->restClient = new RestClient();
    }

    public function index()
    {
        $aData['VIEW'] = 'testing/index';
        $this->layout($aData);
    }

    public function testView()
    {
        $aData['VIEW'] = 'testing/testView';
        $aData['lenguajes'] = array('php', 'java', 'javascript');
        $this->view($aData);
    }

    public function testParametersResource($id = NULL)
    {
        echo 'resource id: ' . $id;
    }

    public function testSession()
    {
        if($_SESSION)
        {
            echo $_SESSION['user'] . ' ' . $_SESSION['rol'];
        }
    }

    public function testSessionDestroy()
    {
        session_destroy();
    }

    public function testRestClient()
    {
        $this->restClient->setUri(array(
            "uri" => "https://jsonplaceholder.typicode.com/posts/"
        ));
        // Trae todos los recursos
//        $result = $this->restClient->fetch();

        // Trae el recurso id = 1
        $result = $this->restClient->fetch(1);

        // Actualizar recurso
//        $result = $this->restClient->save(array(
//            'userId' => 1,
//            'title' => 'Yeaaah!!!'
//        ), 1);

        // crea un recurso
//        $result = $this->restClient->save(array(
//            'userId' => 1,
//            'title'  => 'Yeaaah!!!'
//        ));

        // Elimina un recurso
//        $result = $this->restClient->delete(1);
        print_r($result);
    }

    public function ataqueXSS()
    {
        $aData['VIEW'] = 'testing/testView';
        $aData['lenguajes'] = array('<script>alert(\'XSS\');</script>php', 'java', 'javascript');
        $this->view($aData, TRUE);
    }

    public function upload()
    {
        $aData['VIEW'] = 'testing/upload';
        $this->layout($aData);
    }    

    public function testDoUpload()
    {
        $aAllowed = array(
            'image/gif',
            'image/jpeg',
            'image/jpg',
            'image/png',
        );
        $result = Upload::doUpload($_FILES['archivo'], $aAllowed);
        print_r($result);
    }

    public function testExportToCSV()
    {
        $cols = array('A', 'B', 'C');
        $rows = array(
            array('A1', 'B1', 'C1'),
            array('A2', 'B2', 'C2'),
            array('A3', 'B3', 'C3')
        );
        exportToCSV('prueba', $rows, $cols);
    }
}