<?php
namespace App\Controller;

/**
 * @author aduartem
 */

class TestingVendorsController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {        
        $this->layout();
    }

    public function testExportToExcel()
    {
        $aList = array(
            array(
                'Nombre'    => 'Andres',
                'Apellido'  => 'Duarte',
            ),
            array(
                'Nombre'    => 'Juan',
                'Apellido'  => 'Perez',
            )
        );
        exportToExcel($aList);
    }
}