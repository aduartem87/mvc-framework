<?php
/**
 * AMBIENTE APLICACION
 * Valores: development, qa, production
 */
define('ENVIRONMENT', 'development');

switch(ENVIRONMENT)
{
    case 'development':
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        break;
    case 'qa':
        error_reporting(E_WARNING);
        break;
    case 'production':
        ini_set('display_errors', 0);
        error_reporting(0);
        break;
    default:
        echo 'El ambiente de la aplicación no está configurado correctamente.';
        exit();
}

define('CONTROLLER_DEFAULT', 'Welcome');
define('ACTION_DEFAULT', 'index');

// SECURITY
define('XSS_PROTECTION', FALSE);