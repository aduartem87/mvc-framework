<?php
namespace App\Libs\Spider;

use Framework\Libraries\Scraping\Scraping;
use App\Libs\Spider\Website;

/**
 * @author aduartem
 */

class Paris implements Website
{
    private $_url;
    private $_html;

    public function __construct()
    {
        $this->_url  = "http://www.paris.cl/tienda/es/paris";
        $this->_html = Scraping::getHtml($this->_url);
    }

    public function run()
    {
        $descripcion = $this->_obtenerDescripcion();
        $precio      = $this->_obtenerPrecioOferta();

        $data = $this->_normalizeData($descripcion, array(), 'Descripcion');
        $data = $this->_normalizeData($precio, $data, 'PrecioOferta');
        $col  = array('Descripción', 'Precio Oferta');
        exportToCSV('paris', $data, $col);
    }

    private function _obtenerDescripcion()
    {
        $query  = "//div[@class='scrollPaneDescription']/a/text()";
        $result = Scraping::parse($this->_html, $query);
        return $result;
    }

    private function _obtenerPrecioOferta()
    {
        $query  = "//div[@class='scrollPanePrice']/div[@class='epsot_offerPrice']/text()";
        $result = Scraping::parse($this->_html, $query);
        return $result;
    }

    private function _normalizeData($obj, $data, $key)
    {
        $search = array("\n", "\t");

        $i = 0;
        foreach($obj as $item)
        {
            $valor = trim(str_replace($search, "", $item->nodeValue));

            if( ! empty($valor))
            {
                $data[$i][$key] = $valor;
                $i++;
            }
        }
        return $data;
    }
}
?>