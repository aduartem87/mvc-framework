<?php
namespace App\Libs\Spider;

/**
 * @author aduartem
 */

interface Website
{
    public function run();
}
?>